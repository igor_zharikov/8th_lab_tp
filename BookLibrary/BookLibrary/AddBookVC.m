//
//  AddBookVC.m
//  BookLibrary
//
//  Created by Admin on 11.05.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AddBookVC.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "Book.h"

@interface AddBookVC ()
@property (weak, nonatomic) IBOutlet UITextField *bookNameUI;
@property (weak, nonatomic) IBOutlet UITextField *authorUI;
@property (weak, nonatomic) IBOutlet UITextField *yearUI;
@property (weak, nonatomic) IBOutlet UISlider *ratingUI;

@property (weak, nonatomic) IBOutlet UITextField *publishinUI;
@property (weak, nonatomic) IBOutlet UISwitch *readedUI;
@property (weak, nonatomic) IBOutlet UITextField *numberOfPagesUI;
@property (weak, nonatomic) IBOutlet UIButton *addOrUpdateButton;

@property BOOL update;
@property Book *book;
@property AppDelegate* ad;
@end

@implementation AddBookVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _update = NO;
    _ad = [[AppDelegate alloc] init];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)addBook:(id)sender {
        NSString *author = _authorUI.text;
        NSString *bookName = _bookNameUI.text;
        NSString *year = _yearUI.text;
        NSNumber *numOfPages = [NSNumber numberWithLong:[_numberOfPagesUI.text integerValue]];
        NSDate *date = [NSDate date];
        NSString *publishingHouse = [_publishinUI text];
        NSNumber *rating = [NSNumber numberWithFloat:_ratingUI.value];
        NSNumber *readed = [NSNumber numberWithBool:[_readedUI isOn]];
    if ( !_update){
        [_ad addNewBookWithName:bookName author:author yearPublishing:year numberOfPages:numOfPages dateAdded:date image:nil readed:readed publishingHouse:publishingHouse rating:rating];
        NSLog(@"success adding");
    }
    else{
        NSString *name = _book.name;
        _book.author = author;
        _book.name = bookName;
        _book.yearPublishing = year;
        _book.numberOfPages = numOfPages;
        _book.publishingHouse = publishingHouse;
        _book.rating = rating;
        _book.readed = readed;
        NSLog(@"upd book = %@", _book);
        [_ad saveUpdate:_book name:name];
        NSLog(@"success updating");
    }
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"main"];
    [self presentViewController:vc animated:YES completion:nil];

}

- (IBAction)backButton:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"main"];
    [self presentViewController:vc animated:YES completion:nil];
}

-(void)setBookUI:(Book *)book{
    _update = YES;
    NSLog(@"setBookUI");
    [_addOrUpdateButton setTitle:@"Update book" forState: UIControlStateNormal];
    _authorUI.text = book.author;
    NSLog(@"name = %@", book.name);
    _bookNameUI.text = book.name;
    [_ratingUI setValue:[book.rating doubleValue]];
    _yearUI.text = book.yearPublishing;
    [_readedUI setOn:[book.readed boolValue]];
    _publishinUI.text = book.publishingHouse;
    _numberOfPagesUI.text = [book.numberOfPages stringValue];
    _book = book;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
