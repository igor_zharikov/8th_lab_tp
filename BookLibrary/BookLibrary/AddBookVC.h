//
//  AddBookVC.h
//  BookLibrary
//
//  Created by Admin on 11.05.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "Book.h"

@interface AddBookVC : UIViewController
-(void)setBookUI:(Book*) book;
@end
