//
//  AppDelegate.m
//  TicketBooking
//
//  Created by Admin on 11.05.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
   // NSLog(@"Enter in application");
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"HasLaunchedOnce"]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HasLaunchedOnce"];
    
   /* NSArray *entities = self.managedObjectContext.persistentStoreCoordinator.managedObjectModel.entities;
    NSObject *o = self.managedObjectContext.persistentStoreCoordinator;
    NSLog(@"model = %@", self.managedObjectModel);
    NSLog(@"context = %@", self.managedObjectContext);
    NSLog(@"name = %@", o);
    NSLog(@"size = %@", entities);
    for(NSObject *obj in entities){
        NSLog(@"%@",obj);
    }*/
        [[NSUserDefaults standardUserDefaults] synchronize];
       // NSLog(@"writing data to db");
        Record * firstFlight = [NSEntityDescription insertNewObjectForEntityForName:@"Record" inManagedObjectContext:self.managedObjectContext];
        firstFlight.cityFrom = @"Chelyabinsk";
        firstFlight.cityTo = @"Moscow";
        firstFlight.aviaCompany = @"Aeroflot";
        firstFlight.price = [NSNumber numberWithInt:1000.0];
        Record * secondFlight =[NSEntityDescription insertNewObjectForEntityForName:@"Record" inManagedObjectContext:self.managedObjectContext];
        
        secondFlight.cityFrom = @"Chelyabinsk";
        
        secondFlight.cityTo = @"Moscow";
        
        secondFlight.aviaCompany = @"ChelAvia";
        
        secondFlight.price = [NSNumber numberWithInt:2000.0];
        
        Record * thirdFlight = [NSEntityDescription insertNewObjectForEntityForName:@"Record"
                                
                                                             inManagedObjectContext:self.managedObjectContext];
        
        thirdFlight.cityFrom = @"Ekaterinburg";
        
        thirdFlight.cityTo = @"Ufa";
        thirdFlight.aviaCompany = @"AeroFlot";
        thirdFlight.price = [NSNumber numberWithInt:500.0]; Record * fourthFlight =
        [NSEntityDescription insertNewObjectForEntityForName:@"Record" inManagedObjectContext:self.managedObjectContext];
        fourthFlight.cityFrom = @"Chelyabinsk";
        fourthFlight.cityTo = @"Ufa";
        fourthFlight.aviaCompany = @"RusLine";
        fourthFlight.price = [NSNumber numberWithInt:1500.0]; Record * fivthFlight =
        [NSEntityDescription insertNewObjectForEntityForName:@"Record" inManagedObjectContext:self.managedObjectContext];
        fivthFlight.cityFrom = @"Ekaterinburg";
        fivthFlight.cityTo = @"Moscow";
        fivthFlight.aviaCompany = @"Aeroflot";
        fivthFlight.price = [NSNumber numberWithInt:800.0];
        [self saveContext];
    }
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "-.TicketBooking" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    //NSLog(@"managedObjectModel");
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"TicketBooking" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
   // NSLog(@"persistentStoreCoordinator");
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"TicketBooking.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    //NSLog(@"managedObjectContext");
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
   // NSLog(@"enter in save context");
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSLog(@"saving");
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
          //  NSLog(@"Ошибка!");
            abort();
        }
    }
}
-(NSArray*)getAllFlights

{
   // NSLog(@"enter in getAllFlights");
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
   // NSLog(@"success fetch request");
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Record" inManagedObjectContext:self.managedObjectContext];
    /*if ( entity == nil){
        NSLog(@"entity == nil");
    }
    else{
        NSLog(@"Success getting entity");
        NSLog(@"%@", entity.name);
    }*/
    [fetchRequest setEntity:entity];
  //  NSLog(@"Success setting entity");
    NSError* error;
    NSArray *fetchedRecords =[self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
   // NSLog(@"get records");
    return fetchedRecords;
    
}
@end
